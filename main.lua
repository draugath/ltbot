declare("LTbot", {
	notesid = 58268,
	pending_action = {expel = {}, invite = {}},
	net_votes = 2,
	invite_attempts = 3,
	invite_expiration = 15 * 60, --7 * 24 * 60 * 60,
	reinvite_wait = 5 * 60, --24 * 60 * 60,
	officers_online = {},
	council_online = {},
})

--[[
Features
/guild invite "character name" :: Guild.invite()
/guild expel "character name" :: Guild.expel()
auto invite


Command structure
#lt invite "character name"
#lt expel "character name"
#lt help




Messages from server

Inviting someone not online
Event: CHAT_MSG_SERVER - Data:
        1: (table)      msg="joebobbriggs is not online."

Expelling a character that doesn't exist.
Event: CHAT_MSG_SERVER - Data:
        1: (table)      msg="Unknown character "joebobbriggs""


Expelling someone not in the guild
Event: CHAT_MSG_SERVER - Data:
        1: (table)      msg="That person is not a member of the guild."

		
Expelling someone
Event: GUILD_MEMBER_REMOVED - Data:
        1: (number)     192010 ()
        2: (number)     2
Event: CHAT_MSG_SERVER - Data:
        1: (table)      msg="You have rejected krampus out of the guild."
Event: CHAT_MSG_SERVER_GUILD - Data:
        1: (table)      msg="krampus has been expelled from the guild."

Inviting someone to the guild
Event: CHAT_MSG_SERVER - Data:
        1: (table)      msg="You have invited Ancient Panther to your guild."

Invite declined
Event: CHAT_MSG_SERVER - Data:
        1: (table)      msg="Ancient Panther has declined your invitation to the guild."

Invite accepted
Event: CHAT_MSG_SERVER - Data:
        1: (table)      msg="andre_spadones39 has joined the guild."
Event: GUILD_MEMBER_ADDED - Data:
        1: (number)     364666 (andre_spadones39)
        2: (number)     0
Event: CHAT_MSG_SERVER_GUILD - Data:
        1: (table)      msg="andre_spadones39 joined the guild."

Event: CHAT_MSG_PRIVATE - Data:
        1: (table)      msg="#lt help",name="draugath",faction=1

]]

local command_levels = {
	[1] = {
		['help'] = 'help [command]',
--		['vote'] = 'vote <yes|no> <charname>',
		['list'] = 'list',
	},
	[2] = {
		['invite'] = 'invite <charname>',
--		['noinvite'] = 'noinvite <charname>',
--		['expel'] = 'expel <charname>',
	},
	[3] = {
		['add'] = 'add <charname>',
		['remove'] = 'remove <charname>',
	},
}

local num_levels = #command_levels
local aggregate_command_levels = {}
for i, v in ipairs(command_levels) do
	for j=i, num_levels do
		aggregate_command_levels[j] = aggregate_command_levels[j] or {}
		for key, desc in pairs(v) do
			aggregate_command_levels[j][key] = desc
		end
	end
end

		
local function SendGuildInvite(other, fake)
	test.varprint(' / ', 'SendGuildInvite() :: other', other, 'fake', fake)
	if (fake) then 
		ProcessEvent("CHAT_MSG_SERVER", {msg = other.." is not online."})
	else
		Guild.invite(other)
	end
end

local function SendMsg(msg, dest)
-- 	if (dest == GetPlayerName()) then
		--print('\127FFFFFF'..msg)
-- 	else
 		irc:VOChatPrivate(dest, msg)
-- 		SendChat(msg, "PRIVATE", dest)
-- 		print('\127FFFFFF'..dest.." <- "..msg)
-- 	end
		--irc:VOChatPrivate(dest, msg)
end

local function SaveData()
	local data = {
		proxy = LTbot.proxy,
		votes = LTbot.votes,
		auto_invite = LTbot.auto_invite,
		declined_invite = LTbot.declined_invite,
	}
	SaveSystemNotes(spickle(data), LTbot.notesid)
end

local function IsAuthorized(name)
	for _, officer in ipairs(LTbot.officers_online) do
		if (name == officer[1]) then
			if (officer[2] == Guild.RankCommander) then
				return 3
			else
				return 2
			end
		end
	end
	for _, council in ipairs(LTbot.council_online) do
--		if (name == council) then return 1 end
	end
	return false
end

local function CalculateNetVotes(name, requester)
	local net = 0
	local requesters_vote
	for _, vote in ipairs(LTbot.votes[name]) do
		if (vote[1] == requester) then requesters_vote = vote[2] == 1 and "YES" or "NO" end
		net = net + vote[2]
	end
	return net, #LTbot.votes[name], requesters_vote
end

local function ProcessRequest(request, requester, level)
	--requester = requester:upper()
	level = LTbot.testlevel or level
	requester = LTbot.testname or requester
	
	local cmd, other = request:match("^(%w+)%s?(.*)")
	test.varprint(' / ', ">> requester", requester, 'level', level, 'cmd', cmd, 'avail', aggregate_command_levels[level][cmd] and true)

	if (not aggregate_command_levels[level][cmd]) then return end
	
	if (cmd == "help") then
		if (other ~= "") then
			SendMsg((aggregate_command_levels[level][other] or "No such command"), requester)
		else
			local helpmsg = {}
			for cmd, desc in pairs(aggregate_command_levels[level]) do table.insert(helpmsg, cmd) end
			table.sort(helpmsg)
			helpmsg = table.concat(helpmsg, ', ')
			SendMsg(helpmsg, requester)
		end

	elseif (cmd == "list") then
-- 		local msg = 'Votes :: '
-- 		local list = {}
-- 		for target in pairs(LTbot.votes) do
-- 			local net, total, requesters_vote = CalculateNetVotes(target, requester)
-- 			table.insert(list, target..(requesters_vote and ' ['..requesters_vote..']' or '')..' ('..net..'/'..LTbot.net_votes..') {'..total..'}')
-- 		end
-- 		if (#list > 0) then msg = msg..table.concat(list, ', ') else msg = msg..'<None>' end
-- 		SendMsg(msg, requester)
-- 
		msg = 'Auto-Invites :: '
		list = {}
		for target, info in pairs(LTbot.auto_invite) do
			table.insert(list, target..' ('..info[2]..'/'..LTbot.invite_attempts..')')
		end
		if (#list > 0) then msg = msg..table.concat(list, ', ') else msg = msg..'<None>' end
		SendMsg(msg, requester)

		msg = 'Lieutenant-by-Proxy :: '
		list = {}
		for proxy in pairs(LTbot.proxy) do
			table.insert(list, proxy)
		end
		if (#list > 0) then msg = msg..table.concat(list, ', ') else msg = msg..'<None>' end
		SendMsg(msg, requester)
	
-- 	elseif (cmd == "vote") then 
-- 		if (level == 1) then
-- 			local vote, other = other:match('^(%w+)%s"?([^"]*)')
-- 			vote = vote and vote:upper()
-- 			if (vote == "YES" or vote == "NO") then
-- 				other = strip_whitespace(other)
-- 				local msg
-- 				LTbot.votes[other] = LTbot.votes[other] or {}
-- 				for _, placed_vote in ipairs(LTbot.votes[other]) do
-- 					if (placed_vote[1] == requester) then
-- 						placed_vote[2] = vote == "YES" and 1 or -1
-- 						msg = "Your vote for "..other.." has been changed."
-- 						break
-- 					end
-- 				end
-- 				if (not msg) then
-- 					msg = "Your vote for "..other.." has been recorded."
-- 					table.insert(LTbot.votes[other], {requester, vote == "YES" and 1 or -1})
-- 				end
-- 				SendMsg(msg, requester)
-- 				if (CalculateNetVotes(other) == LTbot.net_votes) then
-- 					LTbot.votes[other] = nil
-- 					SendGuildInvite(other)
-- 				end
-- 				SaveData()
-- 				
-- 			else
-- 				SendMsg('Invalid syntax', requester)
-- 			end
-- 		else
-- 			SendMsg('You must only be a member of council to vote.', requester)
-- 		end
-- 		
	else
		if (other == "") then return end
		other = other:match('^"?([^"]*)')
		other = strip_whitespace(other)
		if (cmd == "add") then
			LTbot.proxy[other] = true
			SaveData()
		elseif (cmd == "remove") then
			LTbot.proxy[other] = nil
			SaveData()
		elseif (cmd == 'noinvite') then
			LTbot.auto_invite[other] = nil
			SaveData()
			SendMsg('Auto-invite removed for '..other, requester)
		else
			-- Store the name of the requester under the name of the targeted individual for responses
			LTbot.pending_action[cmd] = LTbot.pending_action[cmd] or {}
			LTbot.pending_action[cmd][other] = requester
			if (cmd == "invite") then 
				SendGuildInvite(other, true)
			else
				Guild[cmd](other)
			end
		end
	end
end

function LTbot:CHAT_MSG_PRIVATE(event, data)
	local request = data.msg:match("^#lt (.*)")
	local level = IsAuthorized(data.name)
	if (request and level) then
		ProcessRequest(request, data.name, level)
	end
end

function LTbot:CHAT_MSG_SERVER(event, data)
	-- Invite sent (online)
	if (data.msg:match("You have invited .* to your guild.")) then
		local target = data.msg:match("You have invited (.*) to your guild.")
		local requester = self.pending_action['invite'][target]
		if (requester) then 
			SendMsg("Invite sent to "..target, requester)
		end
		
	-- Invite sent (offline)
	elseif (data.msg:match("is not online.")) then
		local target = data.msg:match("^[* ]*(.*) is not online.")
		test.varprint(' / ', 'invite', tostring(self.pending_action['invite']))
		local requester = self.pending_action['invite'][target]
		test.varprint(' / ', '>>>> requester', requester)
		
		self.pending_action['invite'][target] = nil
		local time = os.time()
		self.auto_invite[target] = {time, 0, 0}
		SaveData()
		if (requester) then 
			SendMsg(target.." is not online.  Auto-invite established.", requester)
		end

	-- Invite accepted
	elseif (data.msg:match("has joined the guild.")) then
		local target = data.msg:match("(.*) has joined the guild.")
		test.varprint(' / ', '### invite accepted :: target', target, 'record', self.auto_invite[target])
		self.pending_action['invite'][target] = nil
		self.auto_invite[target] = nil
		SaveData()

	-- Invite declined
	elseif (data.msg:match("has declined your invitation to the guild.")) then
		local target = data.msg:match("(.*) has declined your invitation to the guild.")
		local requester = self.pending_action['invite'][target]
		self.pending_action['invite'][target] = nil
		test.varprint(' / ', '### invite declined :: target', target, 'requester', requester, 'record', self.auto_invite[target])
		if (requester) then
			SendMsg(target..' has declined the guild invite.', requester)
			
		elseif (#self.officers_online > 0) then
			for _, officer in ipairs(self.officers_online) do
				SendMsg(target..' has declined the guild invite.', officer)
			end
		else
			table.insert(self.declined_invite, target)
		end
		self.auto_invite[target] = nil
		SaveData()
		
	-- Expelling someone from the guild
	elseif (data.msg:match("You have rejected .* out of the guild.")) then
		local target = data.msg:match("You have rejected (.*) out of the guild.")
		local requester = self.pending_action['expel'][target]
		if (requester) then
			self.pending_action['expel'][target] = nil
		end
		
	-- Expelling someone not in the guild
	elseif (data.msg:match("That person is not a member of the guild.")) then
		-- Since no name is given in the message, assume any pending_action belongs to this event
		local target, requester = next(self.pending_action['expel'])
		if (target) then
			self.pending_action['expel'][target] = nil
			SendMsg(target..' is not a member of the guild.', requester)
		end

	-- Expelling a character that does not exist
	elseif (data.msg:match('Unknown character ".*"')) then
		local target = data.msg:match('Unknown character "(.*)"')
		local requester = self.pending_action['expel'][target]
		if (requester) then
			self.pending_action['expel'][target] = nil
			SendMsg(target..' is not a valid name.', requester)
		end
	end
end

function LTbot:GUILD_MEMBER_ADDED(event, charid, rank)
	local name = GetPlayerName(charid)
	if (rank == Guild.RankCommander or rank == Guild.RankCouncilLieutenant or rank == Guild.Lieutenant or LTbot.proxy[name]) then
		for _, target in ipairs(self.declined_invite) do
			SendMsg(target..' declined the guild invitation.', name)
		end
		self.declined_invite = {}
		SaveData()
		table.insert(self.officers_online, {name, rank})
	elseif (rank == Guild.RankCouncil) then
		table.insert(self.council_online, name)
	end
end

function LTbot:GUILD_MEMBER_REMOVED(event, charid, reason)
	local name = GetPlayerName(charid)
	for i, officer in ipairs(self.officers_online) do
		if (name == officer[1]) then
			self.officers_online[i] = nil
			break
		end
	end
	for i, council in ipairs(self.council_online) do
		if (name == council) then
			self.council_online[i] = nil
			break
		end
	end
end

function LTbot:PLAYER_ENTERED_GAME()
	local saved_data = unspickle(LoadSystemNotes(LTbot.notesid))
	LTbot.proxy = saved_data.proxy or {}
	LTbot.votes = saved_data.votes or {}
	LTbot.auto_invite = saved_data.auto_invite or {}
	LTbot.declined_invite = saved_data.declined_invite or {}

	for i = 1, GetNumGuildMembers() do
		local charid, rank, name = GetGuildMemberInfo(i)
		test.varprint(' / ', 'name', name, 'rank', rank)
		if (rank == Guild.RankCommander or rank == Guild.RankCouncilLieutenant or rank == Guild.Lieutenant or LTbot.proxy[name]) then
			table.insert(LTbot.officers_online, {name, rank})
		elseif (rank == Guild.RankCouncil) then
			table.insert(LTbot.council_online, name)
		end
	end

end

-- auto_invite{
-- 	[<character name>] = {
-- 		<time added to auto-invite>,
-- 		<number of invites sent>,
-- 		<time of last auto-invite>
-- 	},
-- }

function LTbot.ScanChat(event, data)
	local invite_record = LTbot.auto_invite[data.name]
	test.varprint(' / ', 'ScanChat() :: event', event, 'data', data, 'invite_record', invite_record)
	if (invite_record) then
		local time = os.time()
		-- Remove the auto-invite if it's been too long
		test.varprint(' / ', 'initial', invite_record[1], 'expire', invite_record[1] + LTbot.invite_expiration, 'now', time, 'test', invite_record[1] + LTbot.invite_expiration < time)
		test.varprint(' / ', 'invites', invite_record[2], 'last', invite_record[3], 'next', invite_record[3] + LTbot.reinvite_wait, 'test', invite_record[3] + 24 * 60 * 60 < time)
		if (invite_record[1] + LTbot.invite_expiration < time) then 
			LTbot.auto_invite[data.name] = nil
		else
			-- If reinvite_wait elapsed
			if (invite_record[3] + LTbot.reinvite_wait < time) then
				invite_record[2] = invite_record[2] + 1
				-- clear the auto_invite record if too many attempts made OR set the time of last invite
				if (invite_record[2] >= LTbot.invite_attempts) then 
					LTbot.auto_invite[data.name] = nil
				else
					
					invite_record[3] = time
				end
				SendGuildInvite(data.name)
			end
		end
		SaveData()
	end
end

local registered_events = {
	CHAT_MSG_CHANNEL = LTbot.ScanChat,
	CHAT_MSG_CHANNEL_ACTIVE = LTbot.ScanChat,
	CHAT_MSG_CHANNEL_EMOTE = LTbot.ScanChat,
	CHAT_MSG_CHANNEL_EMOTE_ACTIVE = LTbot.ScanChat,
	CHAT_MSG_PRIVATE = LTbot,
	CHAT_MSG_SERVER = LTbot,
	GUILD_MEMBER_ADDED = LTbot,
	PLAYER_ENTERED_GAME = LTbot,
}

function LTbot.reload()
	print("\127FFFFFFReloading LTbot code...")
	for event, func in pairs(registered_events) do
		UnregisterEvent(func, event)
	end
	dofile('plugins/LTbot/main.lua')
	ProcessEvent("PLAYER_ENTERED_GAME")
end
RegisterUserCommand('lt_reload', LTbot.reload)

for event, func in pairs(registered_events) do
	test.varprint(' / ', event, func)
	RegisterEvent(func, event)
end

